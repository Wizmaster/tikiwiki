{* $Id: admin_profiles_useful_2.tpl 50290 2014-03-12 14:01:56Z xavidp $ *}

<div class="adminWizardIconleft"><img src="img/icons/large/wizard_profiles48x48.png" alt="{tr}Check out some potentially useful profiles{/tr}" title="{tr}Profiles Wizard{/tr}" /></div>
{tr}Check out this other set of potentially useful profiles for your site{/tr}. </br></br>
<div class="adminWizardContent">
<fieldset>
	<legend>{tr}Potentially Useful Profiles (ii){/tr}</legend>
	<table style="width:100%">
	<tr>
	<td style="width:48%">
	<div class="adminWizardIconright"><img src="img/icons/large/i18n48x48.png" alt="{tr}Multilingual Wiki{/tr}" /></div>
	<b>{tr}Multilingual Wiki{/tr}</b> (<a href="tiki-admin.php?profile=Multilingual_Wiki_12x&show_details_for=Multilingual_Wiki_12x&categories%5B%5D={$tikiMajorVersion}.x&repository=http%3a%2f%2fprofiles.tiki.org%2fprofiles&page=profiles&preloadlist=y&list=List#step2" target="_blank">{tr}apply profile now{/tr}</a>)
	<br>
	{tr}This profile allows Tiki content translation, and sets up modules to change language and to display links to page translations with their percentage of completion.{/tr}
	<br/><br/>{tr}The enabled features comprise:{/tr}
	<ul>
	<li>{tr}Multilingual{/tr}, {tr}Translation assistant{/tr}, {tr}Urgent translation notifications{/tr}</li>
    <li>{tr}Multilingual structures{/tr}, {tr}Quantify change size{/tr}, {tr}Multilingual freetags{/tr}</li>
    <li>{tr}Show pages in user's preferred language{/tr}, {tr}Detect browser language{/tr}</li>
	<br/><em>{tr}See also{/tr} <a href="https://doc.tiki.org/Multilingual+Wiki" target="_blank">{tr}Multilingual Wiki in doc.tiki.org{/tr}</a></em>
	</ul>
	</td>
	<td style="width:4%"> 
	&nbsp;
	</td>
	<td style="width:48%">
	<div class="adminWizardIconright"><img src="img/icons/large/profile_wiki_pagemenu48x48.png" alt="{tr}Menu on Wiki page{/tr}" /></div>
	<b>{tr}Menu on Wiki page{/tr}</b> (<a href="tiki-admin.php?profile=Collaborative_Community_Wiki_menupage&show_details_for=Collaborative_Community_Wiki_menupage&categories%5B%5D={$tikiMajorVersion}.x&repository=http%3a%2f%2fprofiles.tiki.org%2fprofiles&page=profiles&preloadlist=y&list=List#step2" target="_blank">{tr}apply profile now{/tr}</a>)<br/>
	<br>
	{tr}This profile sets up a side module with a menu based on a wiki page in the right hand-side column.{/tr} 
	<br/><br/>{tr}With this profile you can:{/tr}
	<ul>
	<li>{tr}use wiki syntax to edit it{/tr}</li>
    <li>{tr}delegate its edition with wiki page permissions{/tr}</li>
    <li>{tr}use plugins to manage conditional display of sections{/tr}</li>
	<br/><em>{tr}See also{/tr} <a href="https://doc.tiki.org/Module+menupage" target="_blank">{tr}Module menupage in doc.tiki.org{/tr}</a></em>
	</ul>
	</td>
	</tr>
	<tr>
	<td style="width:48%">
	<div class="adminWizardIconright"><img src="img/icons/large/profile_random_header_images48x48.png" alt="{tr}Random header images{/tr}" /></div>
	<b>{tr}Random header images{/tr}</b>  (<a href="tiki-admin.php?profile=Random_header_images&show_details_for=Random_header_images&categories%5B%5D={$tikiMajorVersion}.x&repository=http%3a%2f%2fprofiles.tiki.org%2fprofiles&page=profiles&preloadlist=y&list=List#step2" target="_blank">{tr}apply profile now{/tr}</a>)
	<br>
	{tr}This profile adds a module in the top zone that displays a random image from a File Gallery.{/tr}
	{tr}Some sample images to fit the default configuration are also provided as a starting point:{/tr}
	<ul>
	<li>{tr}default configuration uses images at 800x150px resized by the top module parameters to match the header default size{/tr}</li>
    <li>{tr}a different random image is shown at each page load{/tr}</li>
    <li>{tr}elFinder modern file galery manager (with drag & drop capabilities!) is used by default{/tr}
    <li>{tr}you can tweak the module and file gallery defaults as needed for your needs{/tr}</li>
	<br/><em>{tr}See also{/tr} <a href="https://doc.tiki.org/PluginImg" target="_blank">{tr}PluginImg in doc.tiki.org{/tr}</a></em>
	</ul>
	</td>
	<td style="width:4%"> 
	&nbsp;
	</td>
	<td style="width:48%">
	<div class="adminWizardIconright"><img src="img/icons/large/profile_countries_by_region48x48.png" alt="{tr}Timesheet{/tr}" /></div>
	<b>{tr}Countries By Region{/tr}</b> (<a href="tiki-admin.php?profile=Countries_By_Region&show_details_for=Countries_By_Region&categories%5B%5D={$tikiMajorVersion}.x&repository=http%3a%2f%2fprofiles.tiki.org%2fprofiles&page=profiles&preloadlist=y&list=List#step2" target="_blank">{tr}apply profile now{/tr}</a>)
	<br>
	{tr}This profile will create a set of categories and subcategories in your site with the names of countries grouped by these regions:{/tr}
	<ul>
	<li>{tr}Saharan, Sub-Saharan Africa{/tr}</li>
	<li>{tr}Middle East, North Africa{/tr}</li>
	<li>{tr}Asia{/tr}</li>
	<li>{tr}Europe{/tr}</li>
	<li>{tr}North, Central America{/tr}</li>
    <li>{tr}Oceania{/tr}</li>
    <li>{tr}South America{/tr}</li>
	</ul>
	</td>
	</tr>
	</table>
</fieldset>
<br>
</div>
