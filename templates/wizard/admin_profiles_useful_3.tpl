{* $Id: admin_profiles_useful_3.tpl 50290 2014-03-12 14:01:56Z xavidp $ *}

<div class="adminWizardIconleft"><img src="img/icons/large/wizard_profiles48x48.png" alt="{tr}Check out this other set of potentially useful profiles for your site{/tr}" title="{tr}Profiles Wizard{/tr}" /></div>
{tr}Check out this other set of potentially useful profiles for your site{/tr}. </br></br>
<div class="adminWizardContent">
<fieldset>
	<legend>{tr}Potentially Useful Profiles (iii){/tr}</legend>
	<table style="width:100%">
	<tr>
	<td style="width:48%">
	<div class="adminWizardIconright"><img src="img/icons/large/profile_post-it48x48.png" alt="{tr}Post-it Sticky Note{/tr}" /></div>
	<b>{tr}Post-it Sticky Note{/tr}</b> (<a href="tiki-admin.php?profile=Post-it_Sticky_Note&show_details_for=Post-it_Sticky_Note&categories%5B%5D={$tikiMajorVersion}.x&repository=http%3a%2f%2fprofiles.tiki.org%2fprofiles&page=profiles&preloadlist=y&list=List#step2" target="_blank">{tr}apply profile now{/tr}</a>)
	<br>
	{tr}This profile allows to display a sticky note (a "post-it") in your tiki site{/tr}
	<ul>
	<li>{tr}You can move it to another location{/tr}</li>
    <li>{tr}You can customize the contents and which groups of users will see it (by default, only to Admins){/tr}</li>
    <li>{tr}It will be shown for each user of that group until manually closed.{/tr}
		<ul><li>{tr}If your domain includes a subdirectory in the path, you might get the post-it displayed again at each page reload regardless of user choice, until the admin removes it from the admin panels{/tr}</li></ul></li>
	<br/><em>{tr}See also{/tr} <a href="http://doc.tiki.org/Custom+Code+HowTo+-+Post-It+Notes" target="_blank">{tr}Custom code howto Post-it notes in doc.tiki.org{/tr}</a></em>
	</ul>
	</td>
	<td style="width:4%"> 
	&nbsp;
	</td>
	<td style="width:48%">
<!--	<div class="adminWizardIconright"><img src="img/icons/large/wizard_profiles48x48.png" alt="{tr}Profile X{/tr}" /></div>
	<b>{tr}Profile X{/tr}</b> (<a href="tiki-admin.php?profile=Profile_X&show_details_for=Profile_X&categories%5B%5D={$tikiMajorVersion}.x&repository=http%3a%2f%2fprofiles.tiki.org%2fprofiles&page=profiles&preloadlist=y&list=List#step2" target="_blank">{tr}apply profile now{/tr}</a>)<br/>
	<br>
	{tr}This profile allows to {/tr}
	<ul>
	<li>{tr}...{/tr}</li>
    <li>{tr}...{/tr}</li>
    <li>{tr}...{/tr}</li>
	<br/><em>{tr}See also{/tr} <a href="https://doc.tiki.org/Feature_X" target="_blank">{tr}Feature_X in doc.tiki.org{/tr}</a></em>
	</ul>
-->	</td>
	</tr>
	<tr>
	<td style="width:48%">
<!--	<div class="adminWizardIconright"><img src="img/icons/large/wizard_profiles48x48.png" alt="{tr}Profile X{/tr}" /></div>
	<b>{tr}Profile X{/tr}</b> (<a href="tiki-admin.php?profile=Profile_X&show_details_for=Profile_X&categories%5B%5D={$tikiMajorVersion}.x&repository=http%3a%2f%2fprofiles.tiki.org%2fprofiles&page=profiles&preloadlist=y&list=List#step2" target="_blank">{tr}apply profile now{/tr}</a>)<br/>
	<br>
	{tr}This profile allows to {/tr}
	<ul>
	<li>{tr}...{/tr}</li>
    <li>{tr}...{/tr}</li>
    <li>{tr}...{/tr}</li>
	<br/><em>{tr}See also{/tr} <a href="https://doc.tiki.org/Feature_X" target="_blank">{tr}Feature_X in doc.tiki.org{/tr}</a></em>
	</ul>
-->	</td>
	<td style="width:4%"> 
	&nbsp;
	</td>
	<td style="width:48%">
<!--	<div class="adminWizardIconright"><img src="img/icons/large/wizard_profiles48x48.png" alt="{tr}Profile X{/tr}" /></div>
	<b>{tr}Profile X{/tr}</b> (<a href="tiki-admin.php?profile=Profile_X&show_details_for=Profile_X&categories%5B%5D={$tikiMajorVersion}.x&repository=http%3a%2f%2fprofiles.tiki.org%2fprofiles&page=profiles&preloadlist=y&list=List#step2" target="_blank">{tr}apply profile now{/tr}</a>)<br/>
	<br>
	{tr}This profile allows to {/tr}
	<ul>
	<li>{tr}...{/tr}</li>
    <li>{tr}...{/tr}</li>
    <li>{tr}...{/tr}</li>
	<br/><em>{tr}See also{/tr} <a href="https://doc.tiki.org/Feature_X" target="_blank">{tr}Feature_X in doc.tiki.org{/tr}</a></em>
	</ul>
-->	</td>
	</tr>
	</table>
</fieldset>
<br>
</div>
