{* $Id: admin_profiles_demo_2.tpl 50290 2014-03-12 14:01:56Z xavidp $ *}

<div class="adminWizardIconleft"><img src="img/icons/large/wizard_profiles48x48.png" alt="{tr}Check out some profiles{/tr}" title="{tr}Profiles Wizard{/tr}" /></div>
{tr}Check out another set of profiles that demonstrate some other Tiki Features that you might be interested in for your site{/tr}. </br></br>
<div class="adminWizardContent">
<fieldset>
	<legend>{tr}Demo Profiles (ii){/tr}</legend>
	<table style="width:100%">
	<tr>
	<td style="width:48%">
	<div class="adminWizardIconright"><img src="img/icons/large/user_tracker48x48.png" alt="{tr}User & Registration Tracker{/tr}" /></div>
	<b>{tr}User & Registration Tracker{/tr}</b> (<a href="tiki-admin.php?profile=User_Trackers&show_details_for=User_Trackers&categories%5B%5D={$tikiMajorVersion}.x&repository=http%3a%2f%2fprofiles.tiki.org%2fprofiles&page=profiles&preloadlist=y&list=List#step2" target="_blank">{tr}apply profile now{/tr}</a>)
	<br>
	{tr}This profile allows to set up a User Tracker, and you can decide which fields are shown in the registration form of your site, and/or which ones in the User Wizard, when you enable it to display and collect more information to your existing users.{/tr}
	{tr}It includes:{/tr}
	<ul>
	<li>{tr}A long list of predefined usual fields, to choose from{/tr}</li>
    <li>{tr}Some fields already prepared to display custom information from your specific site{/tr}</li>
    <li>{tr}The chance to easily customize it with the power of Trackers{/tr}</li>
	<br/><em>{tr}See also{/tr} <a href="https://doc.tiki.org/User+Tracker" target="_blank">{tr}User Tracker Wiki in doc.tiki.org{/tr}</a></em>
	</ul>
	</td>
	<td style="width:4%"> 
	&nbsp;
	</td>
	<td style="width:48%">
	<div class="adminWizardIconright"><img src="img/icons/large/profile_easy_geoblog48x48.png" alt="{tr}Easy GeoBlog{/tr}" /></div>
	<b>{tr}Easy GeoBlog{/tr}</b> (<a href="tiki-admin.php?profile=Easy+GeoBlog&show_details_for=Easy+GeoBlog&categories%5B%5D={$tikiMajorVersion}.x&repository=http%3a%2f%2fprofiles.tiki.org%2fprofiles&page=profiles&preloadlist=y&list=List#step2" target="_blank">{tr}apply profile now{/tr}</a>)<br/>
	<br>
	{tr}This profile demonstrates the geolocation of Blog posts, in conjunction with other associated features:{/tr}
	<ul>
	<li>{tr}Single map with all geolocated blog posts{/tr}</li>
	<li>{tr}Different home page once the user logs in{/tr}</li>
	<li>{tr}Random header image from files included in a file gallery{/tr}</li>
	<li>{tr}Wysiwyg Editor (compatible mode with wiki syntax){/tr}</li>
	<li>{tr}Wiki, Search, Menu & Freetags{/tr}</li>
	<li>{tr}Comments moderation & Banning (for anonymous comments to your site){/tr}</li>
	<br/><em>{tr}See also{/tr} <a href="https://doc.tiki.org/Geolocation" target="_blank">{tr}Geolocation in doc.tiki.org{/tr}</a></em>
	</ul>
	</td>
	</tr>
	<tr>
	<td style="width:48%">
	<div class="adminWizardIconright"><img src="img/icons/large/profile_slideshow48x48.png" alt="{tr}Slideshow demo{/tr}" /></div>
	<b>{tr}Slideshow demo{/tr}</b>  (<a href="tiki-admin.php?profile=Slideshow_demo&show_details_for=Slideshow_demo&categories%5B%5D={$tikiMajorVersion}.x&repository=http%3a%2f%2fprofiles.tiki.org%2fprofiles&page=profiles&preloadlist=y&list=List#step2" target="_blank">{tr}apply profile now{/tr}</a>)
	<br>
	{tr}This profile sets up a slideshow from a simple wiki page, which you can use to learn the basics of how easily the JqueryS5 slideshow system in Tiki works.{/tr}
	<ul>
	<li>{tr}All content is in a wiki page, which can be printed to your audience in just a few sheets of paper{/tr}</li>
    <li>{tr}Headers of different levels are used as markers of 'new slide' and used as titles{/tr}</li>
    <li>{tr}Many settings can be predefined as parameters of a call to PluginSlideshow{/tr}</li>
    <li>{tr}Allows slide notes in a separate window for dual monitor setups, slide numbers in footer, timer, style with background images, navigation bar with all slides listed to jump to{/tr}</li>
	<br/><em>{tr}See also{/tr} <a href="https://doc.tiki.org/Slideshow" target="_blank">{tr}Slideshow in doc.tiki.org{/tr}</a></em>
	</ul>
	</td>
	<td style="width:4%"> 
	&nbsp;
	</td>
	<td style="width:48%">
	<div class="adminWizardIconright"><img src="img/icons/large/profile_custom_contact_form48x48.png" alt="{tr}Custom Contact Form{/tr}" /></div>
	<b>{tr}Custom Contact Form{/tr}</b> (<a href="tiki-admin.php?profile=Custom_Contact_Form&show_details_for=Custom_Contact_Form&categories%5B%5D={$tikiMajorVersion}.x&repository=http%3a%2f%2fprofiles.tiki.org%2fprofiles&page=profiles&preloadlist=y&list=List#step2" target="_blank">{tr}apply profile now{/tr}</a>)
	<br>
	{tr}This profile eases the task to create a custom contact form adapted to the specific case of that site.{/tr}
	<ul>
	<li>{tr}Enables Trackers and sets up a few fields to create a basic "contact us" form as a starting point{/tr}</li>
    <li>{tr}New fields can be added asking questions specific for the site{/tr}</li>
    <li>{tr}You decide where and when to display the link to the constact us form in your Tiki menus and pages{/tr}</li>
	<br/><em>{tr}See also{/tr} <a href="https://doc.tiki.org/Trackers" target="_blank">{tr}Trackers in doc.tiki.org{/tr}</a></em>
	</ul>
	</td>
	</tr>
	</table>
</fieldset>
<br>
</div>

