{* $Id: admin_profiles_featured.tpl 50464 2014-03-24 01:21:39Z xavidp $ *}

<div class="adminWizardIconleft"><img src="img/icons/large/wizard_profiles48x48.png" alt="{tr}Configuration Profiles Wizard{/tr}" title="{tr}Configuration Profiles Wizard{/tr}" /></div>
{tr}Initialize Tiki as an application, e.g. a blog., by means of applying one of the 4 featured configuration profiles{/tr}. </br></br>
<div class="adminWizardContent">
<fieldset>
	<legend>{tr}Featured Site Profiles{/tr}:</legend>
	<table style="width:100%">
	<tr>
	<td style="width:48%">
	<div class="adminWizardIconright"><img src="img/icons/large/profile_collaborative_community48x48.png" alt="{tr}Collaborative Community{/tr}" /></div>
	<b>{tr}Collaborative Community{/tr}</b> (<a href="tiki-admin.php?profile=Collaborative_Community_12x&show_details_for=Collaborative_Community_12x&categories%5B%5D={$tikiMajorVersion}.x&categories%5B%5D=Featured+profiles&repository=http%3a%2f%2fprofiles.tiki.org%2fprofiles&page=profiles&preloadlist=y&list=List#step2" target="_blank">{tr}apply profile now{/tr}</a>)
	<br>
{tr}Setup to help subject experts and enthusiasts work together to build a Knowledge Base{/tr}
	<div target="tikihelp" class="tikihelp" title="{tr}Collaborative Community{/tr}: 
	<ul>
		<li>{tr}Wiki Editing{/tr}</li>
	    <li>{tr}Personal Member Spaces{/tr}</li>
	    <li>{tr}Forums{/tr}</li>
	    <li>{tr}Blogs{/tr}</li> 
	</ul>">
		<img src="img/icons/help.png" alt="" width="16" height="16" class="icon" />
	</div>
	<div style="display:block; margin-left:auto; margin-right:auto; width:202px;">
		<a href="http://tiki.org/display524" class="internal" rel="box" title="{tr}Click to expand{/tr}">
			<img src="http://tiki.org/display524"  width="100" style="display:block; margin-left:auto; margin-right:auto;border:1px solid darkgray;" alt="Click to expand" class="regImage pluginImg" title="{tr}Click to expand{/tr}" />
		</a>
		<div class="mini" style="width:100px;">
			<div class="thumbcaption">{tr}Click to expand{/tr}</div>
		</div>
	</div>
	</td>
	<td style="width:4%"> 
	&nbsp;
	</td>
	<td style="width:48%">
	<div class="adminWizardIconright"><img src="img/icons/large/profile_company_intranet48x48.png" alt="{tr}Company Intranet{/tr}" /></div>
	<b>{tr}Company Intranet{/tr}</b> (<a href="tiki-admin.php?profile=Company_Intranet_12x&show_details_for=Company_Intranet_12x&categories%5B%5D={$tikiMajorVersion}.x&categories%5B%5D=Featured+profiles&repository=http%3a%2f%2fprofiles.tiki.org%2fprofiles&page=profiles&preloadlist=y&list=List#step2" target="_blank">{tr}apply profile now{/tr}</a>)
	<br>
{tr}Setup for a Corporate Intranet of a typical medium-sized business{/tr}. 
	<div target="tikihelp" class="tikihelp" title="{tr}Company Intranet{/tr}: 
	<ul>
		<li>{tr}Company News Articles{/tr}</li>
	    <li>{tr}Executive Blog{/tr}</li>
	    <li>{tr}File Repository & Management{/tr}</li>
	    <li>{tr}Collaborative Wiki{/tr} </li>
   	</ul>">
		<img src="img/icons/help.png" alt="" width="16" height="16" class="icon" />
	</div>
	<div style="display:block; margin-left:auto; margin-right:auto; width:202px;">
		<a href="http://tiki.org/display525" class="internal" rel="box" title="{tr}Click to expand{/tr}">
			<img src="http://tiki.org/display525"  width="100" style="display:block; margin-left:auto; margin-right:auto;border:1px solid darkgray;" alt="Click to expand" class="regImage pluginImg" title="{tr}Click to expand{/tr}" />
		</a>
		<div class="mini" style="width:100px;">
			<div class="thumbcaption">{tr}Click to expand{/tr}</div>
		</div>
	</div>
	</td>
	</tr>
	<tr>
	<td style="width:48%">
	<div class="adminWizardIconright"><img src="img/icons/large/profile_personal_blog_and_profile48x48.png" alt="{tr}Personal Blog and Profile{/tr}" /></div>
	<b>{tr}Personal Blog and Profile{/tr}</b>  (<a href="tiki-admin.php?profile=Personal_Blog_and_Profile_12x&show_details_for=Personal_Blog_and_Profile_12x&categories%5B%5D={$tikiMajorVersion}.x&categories%5B%5D=Featured+profiles&repository=http%3a%2f%2fprofiles.tiki.org%2fprofiles&page=profiles&preloadlist=y&list=List#step2" target="_blank">{tr}apply profile now{/tr}</a>)
	<br>
{tr}Setup with many cool features to help you integrate the Social Web and establish a strong presence in the Blogosphere{/tr} 
	<div target="tikihelp" class="tikihelp" title="{tr}Personal Blog and Profile{/tr}: 
	<ul>
		<li>{tr}Blog (Full set of blog related features){/tr}</li>
	    <li>{tr}Image Gallery{/tr}</li>
	    <li>{tr}RSS Integration{/tr}</li>
	    <li>{tr}Video Log{/tr}</li> 
	</ul>">
		<img src="img/icons/help.png" alt="" width="16" height="16" class="icon" />
	</div>
	<div style="display:block; margin-left:auto; margin-right:auto; width:202px;">
		<a href="http://tiki.org/display526" class="internal" rel="box" title="{tr}Click to expand{/tr}">
			<img src="http://tiki.org/display526"  width="100" style="display:block; margin-left:auto; margin-right:auto;border:1px solid darkgray;" alt="Click to expand" class="regImage pluginImg" title="{tr}Click to expand{/tr}" />
		</a>
		<div class="mini" style="width:100px;">
			<div class="thumbcaption">{tr}Click to expand{/tr}</div>
		</div>
	</div>
	</td>
	<td style="width:4%"> 
	&nbsp;
	</td>
	<td style="width:48%">
	<div class="adminWizardIconright"><img src="img/icons/large/profile_small_org_web_presence48x48.png" alt="{tr}Small Organization Web Presence{/tr}" /></div>
	<b>{tr}Small Organization Web Presence{/tr}</b> (<a href="tiki-admin.php?profile=Small_Organization_Web_Presence_12x&show_details_for=Small_Organization_Web_Presence_12x&categories%5B%5D={$tikiMajorVersion}.x&categories%5B%5D=Featured+profiles&repository=http%3a%2f%2fprofiles.tiki.org%2fprofiles&page=profiles&preloadlist=y&list=List#step2" target="_blank">{tr}apply profile now{/tr}</a>)
	<br>
{tr}Setup for a Web Presence of a typical small business or non-profit{/tr}. 
	<div target="tikihelp" class="tikihelp" title="{tr}Small Organization Web Presence{/tr}: 
	<ul>
		<li>{tr}Company News & Updates{/tr}</li>
	    <li>{tr}Highlight Company's Products and Services{/tr}</li>
	    <li>{tr}File Gallery (great for Media Kit){/tr}</li>
	    <li>{tr}Contact Form{/tr} </li> 
	</ul>">
		<img src="img/icons/help.png" alt="" width="16" height="16" class="icon" />
	</div>
	<div style="display:block; margin-left:auto; margin-right:auto; width:202px;">
		<a href="http://tiki.org/display527" class="internal" rel="box" title="{tr}Click to expand{/tr}">
			<img src="http://tiki.org/display527"  width="100" style="display:block; margin-left:auto; margin-right:auto;border:1px solid darkgray;" alt="Click to expand" class="regImage pluginImg" title="{tr}Click to expand{/tr}" />
		</a>
		<div class="mini" style="width:100px;">
			<div class="thumbcaption">{tr}Click to expand{/tr}</div>
		</div>
	</div>
	</td>
	</tr>
	</table>
	<br>
	<em>{tr}See also{/tr} <a href="tiki-admin.php?page=profiles&amp;alt=Profiles" target="_blank">{tr}Profiles admin panel{/tr}</a></em>
</fieldset>
<br>
</div>
