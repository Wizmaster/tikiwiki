{* $Id: admin_profiles_useful.tpl 50541 2014-03-28 11:17:08Z jonnybradley $ *}

<div class="adminWizardIconleft"><img src="img/icons/large/wizard_profiles48x48.png" alt="{tr}Check out some potentially useful profiles{/tr}" title="{tr}Configuration Profiles Wizard{/tr}" /></div>
{tr}Check out some potentially useful profiles for your site{/tr}. </br></br>
<div class="adminWizardContent">
<fieldset>
	<legend>{tr}Potentially Useful Profiles{/tr}</legend>
	<table style="width:100%">
	<tr>
	<td style="width:48%">
	<div class="adminWizardIconright"><img src="img/icons/large/profile_mobile48x48.png" alt="{tr}Mobile{/tr}" /></div>
	<b>{tr}Mobile{/tr}</b> (<a href="tiki-admin.php?profile=Mobile&show_details_for=Mobile&categories%5B%5D={$tikiMajorVersion}.x&repository=http%3a%2f%2fprofiles.tiki.org%2fprofiles&page=profiles&preloadlist=y&list=List#step2" target="_blank">{tr}apply profile now{/tr}</a>)
	<br>
	{tr}This profile allows to switch the site layout, text and icons to users with smartphones and tablets{/tr}
	<a href="https://doc.tiki.org/Mobile" target="tikihelp" class="tikihelp" title="{tr}Mobile{/tr}: 
	<ul>
		<li>{tr}Automatic switch when a mobile device is detected{/tr}</li>
	    <li>{tr}Manual switch allowed with provided side module{/tr}</li>
	    <li>{tr}Enhanced mobile mode in Tiki 12{/tr}</li>
	</ul>">
		<img src="img/icons/help.png" alt="" width="16" height="16" class="icon" />
	</a>
	<div style="display:block; margin-left:auto; margin-right:auto; width:202px;">
		<a href="http://doc.tiki.org/display942" class="internal" rel="box" title="{tr}Click to expand{/tr}">
			<img src="http://doc.tiki.org/display942"  width="100" style="display:block; margin-left:auto; margin-right:auto;border:1px solid darkgray;" alt="Click to expand" class="regImage pluginImg" title="{tr}Click to expand{/tr}" />
		</a>
		<div class="mini" style="width:100px;">
			<div class="thumbcaption">{tr}Click to expand{/tr}</div>
		</div>
	</div>
	</td>
	<td style="width:4%"> 
	&nbsp;
	</td>
	<td style="width:48%">
	<div class="adminWizardIconright"><img src="img/icons/large/profile_debug_mode48x48.png" alt="{tr}Debug Mode Enabled{/tr}" /></div>
	<b>{tr}Debug Mode Enabled{/tr}</b> (<a href="tiki-admin.php?profile=Debug_Mode_Enabled&show_details_for=Debug_Mode_Enabled&categories%5B%5D={$tikiMajorVersion}.x&repository=http%3a%2f%2fprofiles.tiki.org%2fprofiles&page=profiles&preloadlist=y&list=List#step2" target="_blank">{tr}apply profile now{/tr}</a>)<br/>
	<b>{tr}Debug Mode Disabled{/tr}</b> (<a href="tiki-admin.php?profile=Debug_Mode_Disabled&show_details_for=Debug_Mode_Disabled&categories%5B%5D={$tikiMajorVersion}.x&repository=http%3a%2f%2fprofiles.tiki.org%2fprofiles&page=profiles&preloadlist=y&list=List#step2" target="_blank">{tr}apply profile now{/tr}</a>)
	<br>
	{tr}Profile <i>Debug_Mode_Enabled</i> will help you detect potential errors and warnings which are hidden otherwise.{/tr}
	{tr}Once applied, you might like to apply the opposite profile: <i>Debug_Mode_Disabled</i>, if not changing the appropriate settings by hand.{/tr}
	<a href="https://dev.tiki.org/Recovery" target="tikihelp" class="tikihelp" title="{tr}Debug Mode Enabled{/tr} & {tr}Debug Mode Disabled{/tr}: 
	<ul>
		<li>{tr}Enables/Disables debugging tools{/tr}</li>
	    <li>{tr}Enables/Disables logging tools{/tr}</li>
	    <li>{tr}Disables/Enables redirections to similar pages{/tr}</li>
	    <li>{tr}Enables/Disables error and warning display to all users, not only admins{/tr} </li> 
	</ul>">
		<img src="img/icons/help.png" alt="" width="16" height="16" class="icon" />
	</a>
	</td>
	</tr>
	<tr>
	<td style="width:48%">
	<div class="adminWizardIconright"><img src="img/icons/large/profile_together48x48.png" alt="{tr}Together{/tr}" /></div>
	<b>{tr}Together{/tr}</b>  (<a href="tiki-admin.php?profile=Together&show_details_for=Together&categories%5B%5D={$tikiMajorVersion}.x&repository=http%3a%2f%2fprofiles.tiki.org%2fprofiles&page=profiles&preloadlist=y&list=List#step2" target="_blank">{tr}apply profile now{/tr}</a>)
	<br>
	{tr}This profile adds a simple wiki page on a side module (using the Module menupage), showing the button to start co-writing with TogetherJS.{/tr} 
	<a href="https://doc.tiki.org/PluginTogether" target="tikihelp" class="tikihelp" title="{tr}Together{/tr}: 
	<ul>
		<li>{tr}Allows cowriting documents in real time{/tr}</li>
		<li>{tr}Allows voice communication in real time while editing{/tr}</li>
	    <li>{tr}Uses the TogetherJS Mozilla widget{/tr}</li>
	</ul>">
		<img src="img/icons/help.png" alt="" width="16" height="16" class="icon" />
	</a>
	<div style="display:block; margin-left:auto; margin-right:auto; width:202px;">
		<a href="http://doc.tiki.org/display842" class="internal" rel="box" title="{tr}Click to expand{/tr}">
			<img src="http://doc.tiki.org/display842"  width="100" style="display:block; margin-left:auto; margin-right:auto;border:1px solid darkgray;" alt="Click to expand" class="regImage pluginImg" title="{tr}Click to expand{/tr}" />
		</a>
		<div class="mini" style="width:100px;">
			<div class="thumbcaption">{tr}Click to expand{/tr}</div>
		</div>
	</div>
	</td>
	<td style="width:4%"> 
	&nbsp;
	</td>
	<td style="width:48%">
	<div class="adminWizardIconright"><img src="img/icons/large/profile_timesheet48x48.png" alt="{tr}Timesheet{/tr}" /></div>
	<b>{tr}Timesheet{/tr}</b> (<a href="tiki-admin.php?profile=Time_Sheet&show_details_for=Time_Sheet&categories%5B%5D={$tikiMajorVersion}.x&repository=http%3a%2f%2fprofiles.tiki.org%2fprofiles&page=profiles&preloadlist=y&list=List#step2" target="_blank">{tr}apply profile now{/tr}</a>)
	<br>
	{tr}This profile allows recording time spent on projects. It creates two trackers: one to hold the time spent, and the other with the project names. Both trackers are linked, so that project names can be chosen when entering items to the timesheet tracker{/tr}. 
	<a href="https://doc.tiki.org/Timesheet" target="tikihelp" class="tikihelp" title="{tr}Timesheet{/tr}: 
	<ul>
	    <li>{tr}Allows to track your time spent on projects{/tr}</li>
		<li>{tr}Timesheet & Projects linked trackers already{/tr}</li>
	    <li>{tr}Customize your project categories{/tr}</li>
	    <li>{tr}Add or edit your timesheet fields as desired{/tr} </li> 
	</ul>">
		<img src="img/icons/help.png" alt="" width="16" height="16" class="icon" />
	</a>
	<a href="https://doc.tiki.org/PluginTimesheet" target="tikihelp" class="tikihelp" title="{tr}Plugin Timesheet{/tr}: 
	<em>{tr}See also{/tr} {tr}Plugin Timesheet in doc.tiki.org{/tr}</em>">
		<img src="img/icons/help.png" alt="" width="16" height="16" class="icon" />
	</a>
	<div style="display:block; margin-left:auto; margin-right:auto; width:202px;">
		<a href="http://tiki.org/display523" class="internal" rel="box" title="{tr}Click to expand{/tr}">
			<img src="http://tiki.org/display523"  width="100" style="display:block; margin-left:auto; margin-right:auto;border:1px solid darkgray;" alt="Click to expand" class="regImage pluginImg" title="{tr}Click to expand{/tr}" />
		</a>
		<div class="mini" style="width:100px;">
			<div class="thumbcaption">{tr}Click to expand{/tr}</div>
		</div>
	</div>
	</td>
	</tr>
	</table>
</fieldset>
<br>
</div>
