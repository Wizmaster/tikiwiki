{* $Id: admin_profiles_demo_3.tpl 50290 2014-03-12 14:01:56Z xavidp $ *}

<div class="adminWizardIconleft"><img src="img/icons/large/wizard_profiles48x48.png" alt="{tr}Check out some profiles{/tr}" title="{tr}Profiles Wizard{/tr}" /></div>
{tr}Check out another set of profiles that demonstrate some other Tiki Features that you might be interested in for your site{/tr}. </br></br>
<div class="adminWizardContent">
<fieldset>
	<legend>{tr}Demo Profiles (iii){/tr}</legend>
	<table style="width:100%">
	<tr>
	<td style="width:48%">
	<div class="adminWizardIconright"><img src="img/icons/large/profile_shopping_cart48x48.png" alt="{tr}Shopping Cart{/tr}" /></div>
	<b>{tr}Shopping Cart{/tr}</b> (<a href="tiki-admin.php?profile=Shopping_Cart&show_details_for=Shopping_Cart&categories%5B%5D={$tikiMajorVersion}.x&repository=http%3a%2f%2fprofiles.tiki.org%2fprofiles&page=profiles&preloadlist=y&list=List#step2" target="_blank">{tr}apply profile now{/tr}</a>)
	<br>
	{tr}This profile provides a Shopping Cart and the corresponding basic payment system.{/tr}
	{tr}Currently this profile uses the PayPal shopping cart rather than the built in Tiki.{/tr}
	{tr}It creates:{/tr}
	<ul>
	<li>{tr}A tracker for products including price, weight, image and stock quantity{/tr}</li>
    <li>{tr}Some sample items which are open, pending and closed items, with different permissions to view or edit them for different groups of users{/tr}</li>
	<li>{tr}A small category subtree to classify products{/tr}</li>
    <li>{tr}Wiki pages to display the available products list, one product details page and a search form{/tr}</li>
	<br/><em>{tr}See also{/tr} <a href="https://doc.tiki.org/Shopping+Cart" target="_blank">{tr}Shopping Cart in doc.tiki.org{/tr}</a></em>
	</ul>
	</td>
	<td style="width:4%"> 
	&nbsp;
	</td>
	<td style="width:48%">
	<div class="adminWizardIconright"><img src="img/icons/large/profile_groupmail48x48.png" alt="{tr}Groupmail{/tr}" /></div>
	<b>{tr}Groupmail{/tr}</b> (<a href="tiki-admin.php?profile=Groupmail&show_details_for=Groupmail&categories%5B%5D={$tikiMajorVersion}.x&repository=http%3a%2f%2fprofiles.tiki.org%2fprofiles&page=profiles&preloadlist=y&list=List#step2" target="_blank">{tr}apply profile now{/tr}</a>)<br/>
	<br>
	{tr}This profile allows to provide a way for a team (a Tiki group) to process email contact requests, save them in contact lists and act on them and record the process in wiki pages{/tr}
	{tr}It creates:{/tr}
	<ul>
	<li>{tr}A tracker for email messages received and store addresses in the Contacts feature{/tr}</li>
    <li>{tr}Webmail configuration to use an account for groupmail{/tr}</li>
    <li>{tr}A side module with markers to indicate who took which message{/tr}</li>
    <li>{tr}A system to review communication logs from that group mail account{/tr}</li>
	<br/><em>{tr}See also{/tr} <a href="https://doc.tiki.org/Groupmail" target="_blank">{tr}Groupmail in doc.tiki.org{/tr}</a></em>
	</ul>
	</td>
	</tr>
	<tr>
	<td style="width:48%">
<!--	<div class="adminWizardIconright"><img src="img/icons/large/wizard_profiles48x48.png" alt="{tr}Profile X{/tr}" /></div>
	<b>{tr}Profile X{/tr}</b> (<a href="tiki-admin.php?profile=Profile_X&show_details_for=Profile_X&categories%5B%5D={$tikiMajorVersion}.x&repository=http%3a%2f%2fprofiles.tiki.org%2fprofiles&page=profiles&preloadlist=y&list=List#step2" target="_blank">{tr}apply profile now{/tr}</a>)<br/>
	<br>
	{tr}This profile allows to {/tr}
	<ul>
	<li>{tr}...{/tr}</li>
    <li>{tr}...{/tr}</li>
    <li>{tr}...{/tr}</li>
	<br/><em>{tr}See also{/tr} <a href="https://doc.tiki.org/Feature_X" target="_blank">{tr}Feature_X in doc.tiki.org{/tr}</a></em>
	</ul>
-->	</td>
	<td style="width:4%"> 
	&nbsp;
	</td>
	<td style="width:48%">
<!--	<div class="adminWizardIconright"><img src="img/icons/large/wizard_profiles48x48.png" alt="{tr}Profile X{/tr}" /></div>
	<b>{tr}Profile X{/tr}</b> (<a href="tiki-admin.php?profile=Profile_X&show_details_for=Profile_X&categories%5B%5D={$tikiMajorVersion}.x&repository=http%3a%2f%2fprofiles.tiki.org%2fprofiles&page=profiles&preloadlist=y&list=List#step2" target="_blank">{tr}apply profile now{/tr}</a>)<br/>
	<br>
	{tr}This profile allows to {/tr}
	<ul>
	<li>{tr}...{/tr}</li>
    <li>{tr}...{/tr}</li>
    <li>{tr}...{/tr}</li>
	<br/><em>{tr}See also{/tr} <a href="https://doc.tiki.org/Feature_X" target="_blank">{tr}Feature_X in doc.tiki.org{/tr}</a></em>
	</ul>
-->	</td>
	</tr>
	</table>
</fieldset>
<br>
</div>

