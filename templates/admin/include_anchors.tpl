{*$Id: include_anchors.tpl 49405 2014-01-16 05:39:16Z chibaguy $*}

{foreach from=$icons key=page item=info}
	{if ! $info.disabled and $info.icon}
		{self_link _icon=$info.icon _icon_class="reflect" _width="32" _height="32" _alt=$info.title page=$page _class="icon tips" _title="`$info.title`|`$info.description`"}{/self_link}
	{/if}
{/foreach}
